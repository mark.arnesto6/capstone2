const mongoose = require('mongoose');

const shirtSchema = new mongoose.Schema({
  name: String,
  description: String,
  price: Number,
  isActive: { type: Boolean, default: true },
  imageFile: {
    type: String,
    validate: {
      validator: function (value) {
        return value.endsWith('.jpg');
      },
      message: props => `${props.value} is not a valid JPG image URL`
    }
  }
});

module.exports = mongoose.model('Shirt', shirtSchema);
