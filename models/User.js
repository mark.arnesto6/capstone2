const mongoose = require('mongoose');

const orderedShirtSchema = new mongoose.Schema({
  shirtId: { type: mongoose.Schema.Types.ObjectId, ref: 'Shirt' },
  shirtName: String,
  quantity: Number,
  totalAmount: Number,
  purchasedOn: { type: Date, default: Date.now },
});

const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  mobileNo: String,
  isAdmin: { type: Boolean, default: false },
  orderedShirts: [orderedShirtSchema],
  shirts: [{
    shirtId: { type: mongoose.Schema.Types.ObjectId, ref: 'Shirt' },
    shirtName: String,
    quantity: Number,
  }],
});

module.exports = mongoose.model('User', userSchema);
