const User = require('../models/User.js');
const Shirt = require('../models/Shirt.js'); // Import Shirt model
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports = {
  checkEmailExists: async (request_body) => {
    try {
      const result = await User.exists({ email: request_body.email });
      return result;
    } catch (error) {
      return {
        message: error.message,
      };
    }
  },

  registerUser: async (request_body) => {
    try {
      const hashedPassword = bcrypt.hashSync(request_body.password, 10);
      const newUser = new User({
        firstName: request_body.firstName,
        lastName: request_body.lastName,
        email: request_body.email,
        mobileNo: request_body.mobileNo,
        password: hashedPassword,
      });

      await newUser.save();
      return {
        message: 'Successfully registered a user!',
      };
    } catch (error) {
      return {
        message: error.message,
      };
    }
  },

  loginUser: async (request, response) => {
    try {
      const user = await User.findOne({ email: request.body.email });
      if (!user) {
        return response.send({
          message: "The user isn't registered yet.",
        });
      }

      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        user.password
      );

      if (isPasswordCorrect) {
        return response.send({ accessToken: auth.createAccessToken(user) });
      } else {
        return response.send({
          message: 'Your password is incorrect',
        });
      }
    } catch (error) {
      response.send(error.message);
    }
  },

  getProfile: async (request_body) => {
    try {
      const user = await User.findOne({ _id: request_body.id });
      if (!user) {
        return {
          message: 'User not found',
        };
      }

      user.password = "";
      return user;
    } catch (error) {
      return {
        message: error.message,
      };
    }
  },

  checkout: async (request, response) => {
    try {
      // Blocks this function if the user is an admin
      if (request.user.isAdmin) {
        return response.send('Action Forbidden');
      }

      let totalPrice = 0;
      const orderShirts = request.user.shirts.map(shirt => {
        const totalAmount = shirt.quantity * shirt.price;
        totalPrice += totalAmount;

        return {
          shirtId: shirt.shirtId,
          shirtName: shirt.shirtName,
          quantity: shirt.quantity,
          totalAmount,
        };
      });

      // Perform checkout logic here...

      // For example, you can add the orderShirts to user's orders
      const updatedUser = await User.findByIdAndUpdate(
        request.user._id,
        { $push: { orders: orderShirts } },
        { new: true }
      );

      response.send({
        message: 'Checkout successful!',
        totalPrice,
        orderDetails: orderShirts,
      });
    } catch (error) {
      response.send(error.message);
    }
  },

  getDetails: async (request, response) => {
    try {
      const user = await User.findById(request.user.id);
      response.send(user.enrollments);
    } catch (error) {
      response.send(error.message);
    }
  },
};
