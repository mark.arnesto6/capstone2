const Shirt = require("../models/Shirt.js");

module.exports = {
  createShirt: async (request, response) => {
    try {
      const newShirt = new Shirt({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
      });

      await newShirt.save();
      response.send(true);
    } catch (error) {
      response.send(false);
    }
  },

  getAllShirts: async (request, response) => {
    const shirts = await Shirt.find({});
    response.send(shirts);
  },

  getAllActiveShirts: async (request, response) => {
    const activeShirts = await Shirt.find({ isActive: true });
    response.send(activeShirts);
  },

  getShirt: async (request, response) => {
    const shirt = await Shirt.findById(request.params.id);
    response.send(shirt);
  },

  updateShirt: async (request, response) => {
    try {
      const updatedShirtDetails = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
      };

      await Shirt.findByIdAndUpdate(request.params.id, updatedShirtDetails);
      response.send({
        message: "Shirt has been updated successfully!",
      });
    } catch (error) {
      response.send({
        message: error.message,
      });
    }
  },

  archiveShirt: async (request, response) => {
    try {
      const updatedShirtStatus = {
        isActive: request.body.isActive,
      };

      await Shirt.findByIdAndUpdate(request.params.shirtId, updatedShirtStatus);
      response.send(true);
    } catch (error) {
      response.send({
        message: error.message,
      });
    }
  },

  activateShirt: async (request, response) => {
    try {
      const shirt = await Shirt.findById(request.params.shirtId);
      if (!shirt) {
        return response.send("No Shirt Found :(");
      }

      shirt.isActive = true;
      await shirt.save();
      response.send(true);
    } catch (error) {
      response.send({ message: error.message });
    }
  },

  searchShirts: async (request, response) => {
    try {
      const shirtName = request.body.shirtName;
      const shirts = await Shirt.find({ name: { $regex: shirtName, $options: 'i' } });
      response.send(shirts);
    } catch (error) {
      response.send({
        message: error.message,
      });
    }
  },
};
