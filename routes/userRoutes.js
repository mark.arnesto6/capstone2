const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

router.post('/check-email', (request, response) => {
  UserController.checkEmailExists(request.body).then((result) => {
    response.send(result);
  });
});

router.post('/register', (request, response) => {
  UserController.registerUser(request.body).then((result) => {
    response.send(result);
  });
});

router.post('/login', (request, response) => {
  UserController.loginUser(request, response);
});

router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
  UserController.getProfile(request.body).then((result) => {
    response.send(result);
  });
});

router.post('/checkout', auth.verify, (request, response) => {
  UserController.checkout(request, response);
});

router.get('/orders', auth.verify, (request, response) => {
  UserController.getDetails(request, response);
});

module.exports = router;

