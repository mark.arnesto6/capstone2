const express = require("express");
const router = express.Router();
const ShirtController = require("../controllers/ShirtController.js");
const auth = require("../auth.js");


router.post("/", auth.verify, auth.verifyAdmin, (request, response) => {
  ShirtController.addShirt(request, response);
});

router.get("/all", (request, response) => {
  ShirtController.getAllShirts(request, response);
});

router.get("/", (request, response) => {
  ShirtController.getAllActiveShirts(request, response);
});

router.get("/:id", (request, response) => {
  ShirtController.getShirt(request, response);
});

router.put("/:id", auth.verify, auth.verifyAdmin, (request, response) => {
  ShirtController.updateShirt(request, response);
});

router.put("/:shirtId/archive", auth.verify, (request, response) => {
  ShirtController.archiveShirt(request, response);
});

router.put("/:shirtId/activate", auth.verify, auth.verifyAdmin, (request, response) => {
  ShirtController.activateShirt(request, response);
});

router.post('/search', (request, response) => {
  ShirtController.searchShirts(request, response);
});

module.exports = router;
